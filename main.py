from exchange_logic.exchange_logic_functions import new_bid_request
from sanic import Sanic
from sanic import json as sanic_json

from utils.utils import create_result

app = Sanic('Exchange_Service')


@app.get("/health")
async def health(request):
    return sanic_json(create_result(result_="This is the Exchange Service Running", status_code=200))


@app.get("/exchange/bid")
async def get_bid(request):
    result = new_bid_request()
    return sanic_json(result)


if __name__ == '__main__':
    app.run('0.0.0.0', port=8070)
