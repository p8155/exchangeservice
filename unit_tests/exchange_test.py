import unittest

from exchange_logic.exchange_logic_functions import new_bid_request


class TestExchangeLogic(unittest.TestCase):
    def test_exchange(self):
        result = new_bid_request()
        print(result)
        self.assertEqual(result.get('status_code'), 200)
        self.assertEqual(result.get('result'), 'FIND_APPROPRIATE_BIDS_SUCCESS')
