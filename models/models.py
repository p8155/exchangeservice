from mongoengine import EmbeddedDocument, StringField, PointField, EmbeddedDocumentField, DynamicDocument, ListField, FloatField


class MobileApp(EmbeddedDocument):
    app_id = StringField()
    app_name = StringField()


class DeviceLocation(EmbeddedDocument):
    country = StringField()
    loc = ListField(FloatField())


class MobileDevice(EmbeddedDocument):
    device_id = StringField()
    device_os = StringField()
    location = EmbeddedDocumentField(DeviceLocation)


class BidRequest(DynamicDocument):
    bid_id = StringField()
    mobile_app_info = EmbeddedDocumentField(MobileApp)
    mobile_device_info = EmbeddedDocumentField(MobileDevice)
