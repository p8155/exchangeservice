import json
import uuid
import requests

from config.config import get_config

c = get_config()


def new_bid_request():
    request_body = {
        "bid_id": str(uuid.uuid4()),
        "mobile_app_info": {
            "app_id": str(uuid.uuid4()),
            "app_name": str(uuid.uuid4())
        },
        "mobile_device_info": {
            "device_id": str(uuid.uuid4()),
            "device_os": "Android",
            "location": {
                "country": "Greece",
                "loc": [37.751, 27.822]
            }
        }
    }
    # print(c.get('bidder_url'))
    response = requests.post(f"http://{c.get('bidder_url')}", data=json.dumps(request_body))
    return json.loads(response.content)
