assets = {}


def set_asset(key: str, value):
    assets[key] = value


def get_asset(key: str):
    return assets.get(key)
